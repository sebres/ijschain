# ijschain.py -- A HexChat script that translates messages from the #tcl
# bridge bots on freenode and Libera Chat.  For a Tcl version see
# <https://www.patthoyts.tk/xchat.html>.  This was written because HexChat no
# longer ships with Tcl add-on support by default.
#
# Copyright (c) 2021 D. Bohdan <https://dbohdan.com/>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import hexchat
import re

__module_name__ = "ijschain"
__module_author__ = "D. Bohdan, Sergey G. Brester"
__module_version__ = "0.4.0"
__module_description__ = "Handle the bridge bots on #tcl"


DEBUG = False

BRIDGE_NICK = re.compile(r"^(?:[iI][js]|libera)chain[_\d]*$")
BRIDGED_MESSAGE = re.compile(r"^[<\xab]?([^>\xbb]+?)[>\xbb]? (.*)$")
BRIDGED_ACTION = re.compile(r"^\*{1,3} [<\xab]?([^>\xbb]+?)[>\xbb]? (.*)$")
ACTION_JOIN_LEAVE = re.compile(
    r"(?:(?P<join>(?:has become available|joins|\*\*\* (?P<user_join>[^ ]+) joins))|"
    r"(?:has left|leaves|\*\*\* (?P<user_left>[^ ]+) leaves)"
    r")$"
)
VOICE_FAKE_NICKS = True

if DEBUG:

    def debug(*args):
        hexchat.prnt(*args)

    def command(s):
        debug(f"  ## COMMAND: {s}")
        hexchat.command(s)


else:

    def debug(*args):
        pass

    command = hexchat.command


def client_name(user, bot):
    """Format fully qualified user client name (with proper IRC-escape for user name)."""
    name = re.sub(r"[^\w\-]+", "_", user)
    user = re.sub(r"[^\w_]+", "-", user)
    return f":{user}!~{name}@bridged/{bot}"


def message_callback(word, word_eol, event_name, attributes):
    """Event handler processing channel message or action."""
    bot = hexchat.strip(word[0], -1, 1)  # Remove colors.

    if not BRIDGE_NICK.match(bot):
        return hexchat.EAT_NONE

    debug(f"## CALLBACK: {repr(bot)} {repr(word)} {repr(event_name)}")

    try:
        match = BRIDGED_MESSAGE.match(word[1])
        if not match:
            return hexchat.EAT_NONE
        user = match[1]
        message = match[2]

        # wrap message to action (/me <action> ...):
        if bot == user:
            match = BRIDGED_ACTION.match(message)
        elif re.match(r"^\*{1,3}$", user):
            match = BRIDGED_MESSAGE.match(message)
        else:
            match = None
        if match:
            user = match[1]
            message = match[2]
            event_name = re.sub(r"\b(?:Message|Msg)\b", "Action", event_name)

        if event_name.startswith("Channel Action"):
            # handle join or left action:
            if handle_available_message(bot, user, message):
                return hexchat.EAT_ALL
        else:
            # consider user is still unknown - simulate join action on first message from user:
            handle_available_message(bot, user, {"join": "auto"})

        # suppress highlighting for own messages processed via bridge:
        if event_name.endswith(" Hilight") and user == hexchat.get_info("nick"):
            event_name = (
                event_name[: -len(" Hilight")]
                .replace("Channel ", "Your ")
                .replace(" Msg", " Message")
            )

        debug(f"  ## EMIT: {event_name}, {user}, {message}")
        hexchat.emit_print(event_name, user, message)
        return hexchat.EAT_ALL
    except BaseException as e:
        hexchat.prnt(f"can't parse {bot} {event_name} `{word_eol[1]}`: {e}")
        if DEBUG:
            import traceback

            hexchat.prnt(traceback.format_exc())

    return hexchat.EAT_NONE


def handle_available_message(bot, user, msg):
    """Process join or leave actions."""
    if not isinstance(msg, dict):
        # Parse action message (detect whether it is join/left)
        msg = ACTION_JOIN_LEAVE.match(msg)
        if not msg:
            return False
        msg = msg.groupdict()

        # Try to obtain user name from message
        for grp, match in msg.items():
            if match and grp.startswith("user_"):
                user = match
                break

    # Try to find user with this nick
    users_list = hexchat.get_list("users")
    for list_user in users_list:
        if list_user.nick == user:
            break
    else:
        list_user = None

    client = client_name(user, bot)
    channel = hexchat.get_info("channel")

    if msg.get("join"):
        debug(
            f"  ## JOIN: {client}, known user: {list_user.__dict__ if list_user else None}"
        )
        # still unknown or known with different client name but real join message:
        if (
            not list_user
            or client != f":{list_user.nick}!{list_user.host}"
            and msg.get("join") != "auto"
        ):
            command(f"RECV {client} JOIN {channel}")
            if VOICE_FAKE_NICKS:
                # Use voice to mark bridged (fake) users.
                command(f"RECV :{bot} MODE {channel} +v {user}")
    else:
        debug(
            f"  ## LEFT: {client}, known user: {list_user.__dict__ if list_user else None}"
        )
        # still unknown or known with this client name:
        if not list_user or client == f":{list_user.nick}!{list_user.host}":
            command(f"RECV {client} QUIT :Bridged {bot} user has left")

    return True


# initialize:
for event in (
    "Channel Action",
    "Channel Action Hilight",
    "Channel Message",
    "Channel Msg Hilight",
):
    hexchat.hook_print_attrs(
        event, message_callback, event, priority=hexchat.PRI_HIGHEST
    )
